#include "vectorKakuro.hpp"

#include <gtest/gtest.h>

// Verifica que la longitud de un Kakuro::Vector inicializado sin argumentos sea cero.
TEST(vectorKakuroConstructorTest, longitudDeVectorVacioEsCero)
{
    Kakuro::Vector kakuroVect;
    ASSERT_EQ(0, kakuroVect.getLongitudVector());
}

// Verifica que la longitud de un Kakuro::Vector con argumentos no sea cero.
TEST(vectorKakuroConstructorTest, longitudDeVectorConElementosNoEsCero)
{
    Kakuro::Vector kakuroVect(10, 3, 0);
    ASSERT_NE(0, kakuroVect.getLongitudVector());
}

// Verifica que no puedan insertarse elementos fuera de rango en Kakuro::Vector
TEST(vectorKakuroManejoErrores, InsertarElementoFueraDeRango)
{
    Kakuro::Vector kakuroVect(10, 3, 0);
    ASSERT_THROW(kakuroVect.modificarVector(1, 4), std::out_of_range);
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}