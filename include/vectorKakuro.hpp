#ifndef VECTOR_KAKURO_H
#define VECTOR_KAKURO_H

#include <algorithm>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <vector>

namespace Kakuro
{
const std::vector<int> numerosValidos{1, 2, 3, 4, 5, 6, 7, 8, 9};

class Vector
{
  public:
    /**
     * @brief Construye un objeto Vector vacío.
     *
     */
    Vector()
    {
    }

    /**
     * @brief Construye un objeto Vector.
     *
     * @param valorSuma Valor de la suma del contenido del vector.
     * @param longitudVector Número de elementos del vector.
     * @param direccionVector Determina si es una fila (0) o columna (1).
     */
    Vector(const int valorSuma, const size_t longitudVector, const int direccionVector) : suma(valorSuma)
    {
        // TODO: Pensar en como procesar los datos de cada vector
        // Inicializamos vector a cero
        vect.resize(longitudVector);
        if (direccionVector)
            direccion = direccionVector;

        // Actualizamos dominio
        _calcularDominio();
    }

    /**
     * @brief Destruye el objeto Vector.
     *
     */
    ~Vector()
    {
    }

    /**
     * @brief Calcula los números válidos a elegir para el vector.
     *
     * @details En Kakuro únicamente pueden utilizarse los números del uno al nueve \f$[1-9]\f$ sin repetirse en cada
     * fila o columna. **Ojo**, las filas o columnas pueden subdividirse por bloques con pistas.
     */
    void calcularDominio()
    {
        _calcularDominio();
    }

    /**
     * @brief Muestra el contenido del vector en consola.
     *
     */
    void mostrarVector()
    {
        std::cout << "Vector: [";
        for (int num : vect)
            std::cout << num << ", ";
        std::cout << "]" << std::endl;
    }

    /**
     * @brief Modifica el contenido del vector.
     *
     * @param numero Número a introducir.
     * @param index  Posición en el vector.
     */
    void modificarVector(const int numero, const int index)
    {
        if (index < 0 || index >= vect.size())
        {
            throw std::out_of_range("Índice está fuera del rango permitido");
        }

        vect[index] = numero;
    }

    /**
     * @brief Obtiene la longitud del vector almacenado.
     *
     * @return int Número de elementos del vector.
     */
    int getLongitudVector()
    {
        return vect.size();
    }

    /**
     * @brief Asigna la dirección que tiene el vector en el juego. 0 para vertical y 1 para horizontal.
     *
     * @param dir Dirección del vector.
     */
    void setDireccionVector(const int dir)
    {
        if (dir < 0 || dir > 1)
            throw std::invalid_argument("Sólo se permite 0 (horizontal) o 1 (vertical).");
        direccion = dir;
    }

    /**
     * @brief Obtiene la dirección actual del vector.
     *
     * @return int Dirección (0 para vertical y 1 para horizontal).
     */
    int getDireccionVector()
    {
        return direccion;
    }

  private:
    // Atributos
    int suma = 0;
    int indexFila = 0;
    int indexColumna = 0;
    int direccion = 0; // 0 = vertical, 1 = horizontal
    std::vector<Vector> vecinos;
    // Dominio
    std::vector<int> dominio;
    // Espacios disponibles (c1, c2,..., cn)
    std::vector<int> vect;

    void _calcularDominio()
    {
        // Suma acumulativa del vector
        int sumaActual;
        sumaActual = std::accumulate(vect.begin(), vect.end(), 0);

        // Si la suma del vector es mayor o igual a 1 -> Calcular dominio
        if (sumaActual)
        {
            // Calcula el máximo y mínimo del vector
            int numMayor = *std::max_element(vect.begin(), vect.end());
            int numMenor = *std::min_element(vect.begin(), vect.end());

            // Diferencia entre conjunto de números válidos [1,...,9] y el vector
            std::vector<int> interseccion;
            interseccion = _diferenciaConjuntos(numerosValidos, vect);

            // Filtra la diferencia con base en el máximo y mínimo = Dominio
            for (int num : interseccion)
            {
                if (num < numMenor || num > numMayor)
                    std::erase(vect, num);
            }

            // Imprime en consola
            std::cout << "Dominio: [";
            for (int num : interseccion)
                std::cout << num << ", ";
            std::cout << "]" << std::endl;
        }
        else // De lo contrario, dominio es [1,...,9]
        {
            dominio = numerosValidos;

            std::cout << "Dominio: [";
            for (int num : dominio)
                std::cout << num << ", ";

            std::cout << "]" << std::endl;
        }
    }

    /**
     * @brief Calcula la diferencia entre dos conjuntos.
     *
     * @details Encuentra los valores que no comparte un conjunto v1 con el conjunto v2.
     *
     * @param v1 Vector que contiene el conjunto 1.
     * @param v2 Vector que contiene el conjunto 2.
     * @return std::vector<int> Vector con valores encontrados.
     */
    std::vector<int> _diferenciaConjuntos(const std::vector<int> &v1, const std::vector<int> &v2)
    {
        std::vector<int> diferencia;

        for (int num : v1)
            if (std::find(v2.begin(), v2.end(), num) == v2.end())
                diferencia.push_back(num);

        // Ordena menor a mayor
        std::sort(diferencia.begin(), diferencia.end());

        return diferencia;
    }

    /**
     * @brief Calcula la intersección entre dos conjuntos.
     *
     * @details Encuentra los valores que comparte un conjunto v1 con el conjunto v2.
     *
     * @param v1 Vector que contiene el conjunto 1.
     * @param v2 Vector que contiene el conjunto 2.
     * @return std::vector<int> Vector con valores encontrados.
     */
    std::vector<int> _interseccionConjuntos(const std::vector<int> &v1, const std::vector<int> &v2)
    {
        std::vector<int> interseccion;

        for (int num : v1)
            if (std::find(v2.begin(), v2.end(), num) != v2.end())
                interseccion.push_back(num);

        // Ordena menor a mayor
        std::sort(interseccion.begin(), interseccion.end());

        return interseccion;
    }
};

} // namespace Kakuro

#endif