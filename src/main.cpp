// TODO: Parsear contenido del archivo de texto
// TODO: Crear util.h con funciones para parseo del juego o incluirlo en vectorKakur.hpp (hay que renombrar el archivo).
#include "vectorKakuro.hpp"
#include <ctype.h>
#include <fstream>
#include <vector>
// #include <iostream>
// #include <string>

const int CHAR_A_INT_OFFSET = 48; // ASCII va de 0 a 48.

int main(int argc, char *argv[])
{
    // Kakuro::Vector vectKa(10, 3, 0);
    // vectKa.calcularDominio();  // Borrar funcion cuando no se necesite mas
    // vectKa.mostrarVector();
    // vectKa.modificarVector(1, 0);
    // vectKa.modificarVector(9, 2);
    // vectKa.mostrarVector();
    // vectKa.calcularDominio(); // Borrar funcion cuando no se necesite mas

    // Vector de vectoresKakuro
    std::vector<Kakuro::Vector> contenedorVectoresKakuro;
    // Dimensiones
    int m = 0;
    int n = 0;

    // Validar la cantidad de argumentos
    if (argc != 2)
    {
        std::cerr << "Uso: " << argv[0] << " <ruta_archivo>" << std::endl;
        return 1;
    }

    // Leer archivo
    std::ifstream kakuroFile(argv[1], std::ios::in);

    if (!kakuroFile.is_open())
    {
        std::cerr << "¡El archivo " << argv[1] << " no existe! Verifique la ruta." << std::endl;
        return 1;
    }

    // Mostrar contenido y parseo
    std::string linea;
    bool procesandoHeader = true;
    int indexFilas = 0;
    int indexColumnas = 0;

    std::cout << "Solucionador de Kakuro v1.0" << std::endl;
    while (std::getline(kakuroFile, linea))
    {
        // Lee header
        if (procesandoHeader)
        {
            size_t pos = linea.find('x') + 1; // Sumamos para ajustar subtr
            m = stoi(linea.substr(0, linea.length() - (pos)));
            n = stoi(linea.substr(pos));
            std::cout << "Las dimensiones del tablero son: " << m << 'x' << n << std::endl;
            // std::cout << caracter << ',';
            procesandoHeader = false;
            continue;
        }
        // Print linea
        std::cout << linea << std::endl;

        // Parseo juego
        bool cambioDireccion = true;
        for (int i = 0; i < linea.length(); i++)
        {
            char caracter = linea[i];
            int num = -CHAR_A_INT_OFFSET;

            if (caracter == ' ')
            {
                // Cuenta columnas
                if (linea[i + 1] != ' ')
                    indexColumnas++;
                else // Salta espacios vacíos
                    continue;
            }

            // Procesa números
            if (isdigit(caracter)) // num?
            {
                // Checamos direccion vertical u horizontal
                if (linea[i - 1] == ' ')
                    cambioDireccion = false;
                else
                    cambioDireccion = true;
                // std::cout << "caracter: " << caracter << std::endl;
                num = num + (int)caracter;
                int j = 1;
                while (isdigit(linea[i + j])) // vecino num?
                {
                    if (linea[i + j] != '0') // (int)'0' no es = 0
                        num = num * 10 + (int)linea[i + 1] - CHAR_A_INT_OFFSET;
                    else
                        num = num * 10;

                    j++;
                }
                i = i + j;
                char nuevaPos = linea[i];

                std::cout << "Número encontrado: " << num
                          << " con direccion: " << (cambioDireccion ? "horizontal" : "vertical") << std::endl;
                std::cout << "Nueva posicion: " << nuevaPos << std::endl;
            }
        }

        // IMPRESIÓN DEBUG
        std::cout << std::endl;
        // Fila actual
        std::cout << "Número de fila: " << indexFilas << std::endl;
        // Columnas totales
        std::cout << "Número de columnas: " << indexColumnas << std::endl;
        // Cuenta filas
        indexFilas++;
        indexColumnas = 0;
    }

    kakuroFile.close();

    return 0;
}