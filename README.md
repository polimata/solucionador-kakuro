# Solucionador de Kakuro
Siempre tuve curiosidad por los solucionadores de *sudoku*, pero nunca implementé uno. En vista de la vasta cantidad de proyectos existentes, opté por implementar un solucionador de *kakuro*, un juego con reglas similares.

## ¿Kakuro?
*kakuro* (カックロ en japonés) es un juego de lógica matemática que combina aspectos del *sudoku* y el crucigrama. Las reglas son sencillas:

1. Sólo se pueden utilizar números del uno al nueve (1 - 9).
2. No se pueden repetir los números en cada fila o columna.
3. Los números de cada fila o columna debe sumar un número dado.

### Ejemplo obtenido de [Wikipedia](https://en.wikipedia.org/wiki/Kakuro)
Problema

![Kakuro sin resolver](./media/Kakuro_black_box.svg)

Solución

![Kakuro resuelto](./media/Kakuro_black_box_solution.svg)

